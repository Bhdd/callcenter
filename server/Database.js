"use strict";
exports.__esModule = true;
var Agent_1 = require("./Database/Agent");
exports.Agents = Agent_1["default"];
var Queue_1 = require("./Database/Queue");
exports.Queues = Queue_1["default"];
var AgentQueue_1 = require("./Database/AgentQueue");
exports.AgentQueues = AgentQueue_1["default"];
var QueueCalls_1 = require("./Database/QueueCalls");
exports.QueueCalls = QueueCalls_1["default"];
var AgentStatus_1 = require("./Database/AgentStatus");
exports.AgentStatus = AgentStatus_1["default"];
