import * as DB from "./Database";
import {RandomNumber} from "./Helper";
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
app.use(express.static('app'));
server.listen(80);
var messages = [];
// CODE HERE
const WebsocketEmitter = ()=> { 
 io.on('connection',function(socket){
  setInterval(() => {
    if(messages.length>0) socket.emit("messages",messages);
    },1000)
    });
  }

interface iEvent {
  id: number;
  event: "RINGING" | "ANSWER" | "HANGUP";
  queue_id: number;
  agent_id?: string;
}

const eventHandler = (payload: iEvent) => {
  console.log(payload);
 try {
 var index=payload.queue_id-1;
 var agentIndex=parseInt(payload.agent_id);
 var hasagent=false;
 if(agentIndex>0){agentIndex-=1;hasagent=true;}
  switch(payload.event) { 
    case "HANGUP": { 
      DB.QueueCalls[index].call_finished+=1;
     if( DB.QueueCalls[index]>0) DB.QueueCalls[index].call_waiting-=1;
     if( DB.QueueCalls[index]>0) DB.QueueCalls[index].call_ongoing-=1;
     if(hasagent){
     DB.AgentStatus[agentIndex].calltaken+=1;
     DB.AgentStatus[agentIndex].status="Waiting for call";
     }
       break; 
    } 
    case "RINGING": { 
      DB.QueueCalls[index].call_waiting+=1;
       break; 
    } 
    case "ANSWER": { 
      DB.QueueCalls[index].call_ongoing+=1;
      if( DB.QueueCalls[index]>0) DB.QueueCalls[index].call_waiting-=1;
      if(hasagent){
      DB.AgentStatus[agentIndex].calltaken+=1;
      DB.AgentStatus[agentIndex].status="In Call";
      }
      break; 
   } 
    default: { 
    
       break; 
    } 
 }
} catch (error) {
console.log(error);
} 
 messages=[ DB.QueueCalls,DB.AgentStatus];
};



// DO NOT CHANGE
const CallSimulator = () => {
  let CallID = 0;
  const TOTAL_INDEX = DB.AgentQueues.length;
  setInterval(() => {
    const AgentQueue = DB.AgentQueues[RandomNumber(TOTAL_INDEX)];
    let payload: iEvent = {
      id: CallID++,
      event: "RINGING",
      queue_id: AgentQueue.queue_id
    };

    eventHandler(payload);

    let TIMEOUT = RandomNumber(5, 1);

    setTimeout(() => {
      payload.event = "ANSWER";
      payload.agent_id = AgentQueue.agent_id;
      eventHandler(payload);
    }, TIMEOUT * 1e3);

    TIMEOUT += RandomNumber(15, 5);
    setTimeout(() => {
      payload.event = "HANGUP";
      payload.agent_id = AgentQueue.agent_id;
      eventHandler(payload)
    }, TIMEOUT * 1e3);


  }, 250);
};

CallSimulator();
WebsocketEmitter();