"use strict";
exports.__esModule = true;
exports.RandomNumber = function (end, start) {
    if (start === void 0) { start = 0; }
    return Math.floor(Math.random() * end) + start;
};
