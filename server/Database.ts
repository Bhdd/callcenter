import Agents from "./Database/Agent";
import Queues from "./Database/Queue";
import AgentQueues from "./Database/AgentQueue";
import QueueCalls from "./Database/QueueCalls";
import AgentStatus from "./Database/AgentStatus";

export {
  Agents,
  Queues,
  AgentQueues,
  QueueCalls,
  AgentStatus
}