import Agents from "./Agent";
import Queues from "./Queue";

let AgentQueues = [];

Agents.forEach((Agent) => {
  Queues.forEach(Queue => {
    if (Math.random() > .5)
      AgentQueues.push({
        agent_id: Agent.id,
        queue_id: Queue.id
      })
  })
});
export default AgentQueues;