"use strict";
exports.__esModule = true;
var Agent_1 = require("./Agent");
var AgentStatus = [];
Agent_1["default"].forEach(function (Agent) {
    AgentStatus.push({
        id: Agent.id,
        name: Agent.name,
        status: "Waiting for call",
        calltaken: 0
    });
});
exports["default"] = AgentStatus;
