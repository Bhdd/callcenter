"use strict";
exports.__esModule = true;
var Queue_1 = require("./Queue");
var QueueCalls = [];
Queue_1["default"].forEach(function (Queue) {
    QueueCalls.push({
        queue_id: Queue.id,
        queue_name: Queue.name,
        call_waiting: 0,
        call_ongoing: 0,
        call_finished: 0
    });
});
exports["default"] = QueueCalls;
