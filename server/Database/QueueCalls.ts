import Queues from "./Queue";
let QueueCalls = [];
Queues.forEach(Queue => {
    QueueCalls.push({
        queue_id: Queue.id,
        queue_name:Queue.name,
        call_waiting:0,
        call_ongoing:0,
        call_finished:0
      })
  });
export default QueueCalls;
